const graphql = require('graphql');
const _ = require('lodash');
const Book = require('../models/book');
const Author = require('../models/author');


const {
    GraphQLObjectType,
    GraphQLString, // for fields string type
    GraphQLSchema, // for schema 
    GraphQLID, // for number/string type
    GraphQLInt, // for number type
    GraphQLList, // for List type
    GraphQLNonNull // not allow null value


} = graphql;

// dummy data  for books
// var books = [
//     { name: 'Game of thrones', genre: 'Fantacy', id: '1', authorid: '1' },
//     { name: 'One man army', genre: 'War', id: '2', authorid: '2' },
//     { name: 'Have a fun', genre: 'Entertainment', id: '3', authorid: '4' },
//     { name: 'Love you', genre: 'Love', id: '4', authorid: '3' },
//     { name: 'The Evil', genre: 'Fantacy', id: '5', authorid: '4' },
//     { name: 'Dark night', genre: 'War', id: '6', authorid: '2' },
//     { name: 'fun in city', genre: 'Entertainment', id: '7', authorid: '3' },
//     { name: 'Crush on GOD', genre: 'Love', id: '8', authorid: '2' }
// ];

// // dummy data Authors
// var authors = [
//     { name: 'Peter Jonson', age: 25, id: '1' },
//     { name: 'Manpreet Singh', age: 30, id: '2' },
//     { name: 'Jaspreet Singh', age: 35, id: '3' },
//     { name: 'Baljeet Singh', age: 28, id: '4' },
// ];


const BookType = new GraphQLObjectType({
    name: 'Book',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        genre: { type: GraphQLString },
        author: {
            type: AuthorType,
            resolve(parent, agrs) {

                // return _.find(authors, { id: parent.authorid });
                return Author.findById(parent.authorid);
            }
        }
    })
});

const AuthorType = new GraphQLObjectType({
    name: 'Author',
    fields: () => ({
        id: { type: GraphQLID },
        age: { type: GraphQLInt },
        name: { type: GraphQLString },
        books: {
            type: new GraphQLList(BookType), // To get list books whose is BookType 
            resolve(parent, args) {

                // return _.filter(books, { authorid: parent.id })
                return Book.find({authorid:parent.id});
            }
        }

    })
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        book: {
            type: BookType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {
                // code to get data from db/other source

                //to the what GraphQLId ineternal datatype
                //  console.log(typeof (args.id));

                // return _.find(books, { id: args.id });
                return Book.findById(args.id);

            }
        },
        author: {
            type: AuthorType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args) {

                // return _.find(authors, { id: args.id });
                return Author.findById(args.id);
            }
        },
        books: {
            type: new GraphQLList(BookType),
            resolve(parent, agrs) {
                // return books;
                return Book.find({});
            }
        },
        authors: {
            type: new GraphQLList(AuthorType),
            resolve(parent, args) {
                // return authors
                return Author.find({});
            }
        }
    }
});


const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addAuthor: {
            type: AuthorType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                age: { type: new GraphQLNonNull(GraphQLInt) }
            },
            resolve(parent,args){
                let author=Author({
                    name:args.name,
                    age:args.age
                });

                // Code to add and return new author
              return  author.save();
            }
        },
        addBook:{
            type:BookType,
            args:{
                name:{type:new GraphQLNonNull(GraphQLString)},
                genre:{type: new GraphQLNonNull(GraphQLString)},
                authorid:{type:new GraphQLNonNull(GraphQLID)}
            },
            resolve(parent,args){
                let book=new Book({
                  name:args.name,
                  genre:args.genre,
                  authorid:args.authorid
                });

                // To add and return new book
                return book.save();
            }
        }
    }

});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation:Mutation
})