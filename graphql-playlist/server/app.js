const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

// Allow cross-origin requests
app.use(cors());

mongoose.connect('mongodb://manpreet:test123@ds225442.mlab.com:25442/gql-mydb');
mongoose.connection.once('open', () => {
    console.log("Connected to database");
});


app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen(4000, () => {
    console.log("Now listening for requests on port 4000");
});